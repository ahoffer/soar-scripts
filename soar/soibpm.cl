#SOIbpm.cl created by Nathan De Lee 05/16/2006 all rights reserved
#This script creates bad pixel masks for the SOI imager.

procedure soibps (input)

string input  {"",prompt="Input images"}
string bpmdir {"./BPM/",prompt="BPM directory"}
int satlimit  {30000,prompt="Saturation limit"}
struct *inlist

begin
string intemp,imgname,bname
int len,i

#Get list of images
intemp = mktemp("temp")
files (input, sort=no, > intemp)
inlist = intemp	

#Make sure MSCRED is loaded
if (!defpac("mscred")) {
        print("Package MSCRED is needed by soibpm...please load it.")
        bye()
}
#Make sure IMAGE is loaded
if (!defpac("image")) {
        print("Package IMAGE is needed by soibpm...please load it.")
        bye()
}
while (fscan (inlist, imgname) !=EOF) {

        #Check to see if images exists
        if(!imaccess(imgname)) {
                print (" File: "//imgname//" not found.")
                next;
        }
#Make Directory
		
		if(!access(bpmdir)) {
			mkdir(bpmdir) 
		}
#Copy base image to BPM directory
		len = strldx(".",imgname)
		bname = substr(imgname,1,len-1)
		for(i=1;i<=4;i=i+1) { 
			if(access(bpmdir//bname//"_"//i//".pl")) {
			delete(bpmdir//bname//"_"//i//".pl",verify=no)
			}
		}
		if(imaccess(bpmdir//imgname)) {
			delete(bpmdir//imgname,verify=no)
		}
		copy (imgname,bpmdir//imgname,verbose=no)
#Split BPM file
		mscsplit(bpmdir//imgname,delete=yes,verbose=no)
		delete(bpmdir//bname//"_0.fits")
#Make all pixels below satlimit 0 and above 1	
		for(i=1;i<=4;i=i+1) { 
			imreplace(bpmdir//bname//"_"//i//".fits",0,imagina=0,lower=INDEF,\
					   upper=satlimit,radius=0)
			imreplace(bpmdir//bname//"_"//i//".fits",1,imagina=0,lower=1,\
					   upper=INDEF,radius=5)
			if(access(bpmdir//bname//"_"//i//".pl")) {
				delete(bpmdir//bname//"_"//i//".pl",verify=no)
			}
			rename(bpmdir//bname//"_"//i//".fits","pl",field="extn")
			hedit(imgname//"["//i//"]","bpm",bpmdir//bname//"_"//i//".pl",\
				   verify=no,add=yes,addonly=no,show=no,delete=no,update=yes)
		}
	}
#Clean things up
inlist=""
delete(intemp,verify=no)
end
