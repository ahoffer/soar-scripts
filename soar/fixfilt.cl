#Fixfilt.cl created by Nathan De Lee 04/01/2006 all rights reserved
#The SOAR telescope does not currently write the names of the filters
#into a keyword that is useable by ccdproc.  This script allows the user
#to define the filters in each filter wheel

procedure fixfilt (input)
string input {"",prompt="Input images"}
string oldhead {"FILPOS",prompt="Keyword with Filter Positions"}
string newhead {"FILTER",prompt="New Header Keyword"}
string w1filt1 {"Clear",prompt = "Label: Wheel 1 Filter 1"}
string w1filt2 {"B",prompt = "Label: Wheel 1 Filter 2"}
string w1filt3 {"V",prompt = "Label: Wheel 1 Filter 3"}
string w1filt4 {"R",prompt = "Label: Wheel 1 Filter 4"}
string w1filt5 {"I",prompt = "Label: Wheel 1 Filter 5"}
string w2filt1 {"Clear",prompt = "Label: Wheel 2 Filter 1"}
string w2filt2 {"g",prompt = "Label: Wheel 2 Filter 2"}
string w2filt3 {"r",prompt = "Label: Wheel 2 Filter 3"}
string w2filt4 {"z",prompt = "Label: Wheel 2 Filter 4"}
string w2filt5 {"U",prompt = "Label: Wheel 2 Filter 5"}

struct *inlist

begin
int wheel,filter
string intemp,imgname,wheel1,wheel2,id1,id2,newid

#Get list of images
intemp = mktemp("temp")
files (input, sort=no, > intemp)
inlist = intemp

#Make sure MSCRED is loaded
if (!defpac("mscred")) {
	print("Package MSCRED is needed by fixfilt...please load it.")
	bye()
}

while (fscan (inlist, imgname) !=EOF){

	#Check to see if images exists
	if(!imaccess(imgname)) {
		print (" File: "//imgname//" not found.")
		next;
	}
	
	#Get filter positions
	imgets (imgname//"[0]",param=oldhead)
	print (imgets.value) | scan(wheel1,wheel2)
	
	#Convert position to filterid
	if(wheel1 == "1") { id1 = w1filt1}
	if(wheel1 == "2") { id1 = w1filt2}
	if(wheel1 == "3") { id1 = w1filt3}
	if(wheel1 == "4") { id1 = w1filt4}	
	if(wheel1 == "5") { id1 = w1filt5}

 	if(wheel2 == "1") { id2 = w2filt1}
	if(wheel2 == "2") { id2 = w2filt2}
	if(wheel2 == "3") { id2 = w2filt3}
	if(wheel2 == "4") { id2 = w2filt4}	
	if(wheel2 == "5") { id2 = w2filt5}

	if(!(id1 == "Clear" || id2 == "Clear")) {
		print("Neither wheel position "//id1//" nor "//id2//" is equal to Clear!")
		next
	}

	#Write the non-clear filter to the header
	if(id1 == "Clear") {newid = id2}
	if(id2 == "Clear") {newid = id1}
	
	ccdhedit (imgname,newhead,newid,type="string")

	#Write info to screen if verbose or just to logfile if not
	if(mscred.verbose) {
		print (imgname//" Wheel1: "//id1//" Wheel2: "//id2, >> logfile)
		print (imgname//" "//newhead//" => "//newid, >> logfile)
	}	
	#Write to logfile
	print (imgname//" Wheel1: "//id1//" Wheel2: "//id2, >> mscred.logfile)
	print (imgname//" "//newhead//" => "//newid, >> mscred.logfile)
}

#Clean things up
delete(intemp,verify=no)
end
