def standard_flux(filter=None, standard=None, filter_wave_scale=1.0):
    """Determine standard star flux calibration.
    
    Keyword Arguments:
        filter -- Complete filename for the filter transmission profile
        standard -- Complete filename for standard star flux data file in units
                    erg cm-2 s-1 AA-1 * 10**16
        filter_wave_scale -- Scale wavelength to Angstroms.
                             E.g. nm would be 0.1
    """
    from scipy import interpolate
    import numpy as np
    import matplotlib.pyplot as plt
    filter_wave = np.array([])
    filter_per = np.array([])
    f_filter = open(filter)
    
    for line in f_filter:
        line = line.split()
        filter_wave = np.append(filter_wave, float(line[0])/filter_wave_scale)
        filter_per = np.append(filter_per, float(line[1])/100.)
    
    standard_wave = np.array([])
    standard_flux = np.array([])
    f_standard = open(standard)
    
    for line in f_standard:
        line = line.split()
        standard_wave = np.append(standard_wave, float(line[0]))
        # erg cm-2 s-1 AA-1 * 10**16
        standard_flux = np.append(standard_flux, float(line[1]))
        
    # standard flux is matched to the filter positions
    standard_interp = interpolate.interp1d(standard_wave, standard_flux)
    standard_flux_interp = standard_interp(filter_wave)

    plt.plot(filter_wave, standard_flux_interp, 'o', standard_wave, standard_flux, '-')
    plt.xlim([filter_wave[0]*0.95, filter_wave[-1]*1.05])
    plt.show()
    
    # total flux in erg cm-2 s-1
    return np.sum(standard_flux_interp*filter_wave*filter_per*1.0e-16)


def main():
    tot_flux_Ha = standard_flux(filter='E:/Science/OBS/StandardStars/7580-85.dat', filter_wave_scale=1.0, standard='E:/Science/OBS/StandardStars/fltt7379.dat')
    print("Standard star Halpha flux is "+str(tot_flux_Ha)+" erg cm-2 s-1")

    tot_flux_cont = standard_flux(filter='E:/Science/OBS/StandardStars/7384-84.dat', filter_wave_scale=1.0, standard='E:/Science/OBS/StandardStars/fltt7379.dat')
    print("Standard star continuum flux is "+str(tot_flux_cont)+" erg cm-2 s-1")

    # Flux calibrate fits file
    import pyfits
    import numpy as np
    PI = np.pi
    x_cen = 1495.0
    y_cen = 968.0
    radius = 40.0
    area = PI*radius**2
    bg_radius = 40.0
    bg_area = PI*bg_radius**2
    standard_Ha_exp = 15.0
    standard_cont_exp = 15.0

    #to prevent bg cen from being on the chip gap
    x_bg_cen = 1368.0
    y_bg_cen = 892.0

    y, x = np.ogrid[0:2048, 0:2099]
    mask = ((y - y_cen)**2 + (x - x_cen)**2) > radius**2
    bg_mask = ((y - y_bg_cen)**2 + (x - x_bg_cen)**2) > bg_radius**2

    standard_image_Ha = pyfits.open('E:/Science/OBS/SethOBS/LTT7379/mOBJ.066.fits')
    standard_image_Ha_bg = standard_image_Ha

    standard_image_Ha_bg[0].data[bg_mask] = 0
    tot_counts_Ha_bg = np.sum(standard_image_Ha_bg[0].data)

    standard_image_Ha[0].data[mask] = 0
    tot_counts_Ha = np.sum(standard_image_Ha[0].data)
    net_counts_Ha = tot_counts_Ha - (tot_counts_Ha_bg/bg_area)*area  # correct bg counts to net area

    standard_scaling_Ha = tot_flux_Ha*standard_Ha_exp/net_counts_Ha  # erg cm-2 / counts
    standard_image_Ha[0].data = standard_image_Ha[0].data * standard_scaling_Ha
    standard_image_Ha[0].header.add_comment('Flux calibrated by factor '+str(standard_scaling_Ha))
    standard_image_Ha.writeto('E:/Science/OBS/SethOBS/LTT7379/mOBJ.066_flux.fits', clobber=True)

    # in image units but in the form [y,x]
    standard_image_cont = pyfits.open('E:/Science/OBS/SethOBS/LTT7379/mOBJ.068.fits')
    standard_image_cont_bg = standard_image_cont

    standard_image_cont[0].data[mask] = 0
    standard_image_cont_bg[0].data[bg_mask] = 0

    tot_counts_cont = np.sum(standard_image_cont[0].data)
    tot_counts_cont_bg = np.sum(standard_image_cont_bg[0].data)
    net_counts_cont = tot_counts_cont - (tot_counts_cont_bg/bg_area)*area
    standard_scaling_cont = tot_flux_cont*standard_cont_exp/net_counts_cont  # erg cm-2 / counts
    standard_image_cont[0].data = standard_image_cont[0].data*standard_scaling_cont  # need airmass correction
    standard_image_cont[0].header.add_comment('Flux calibrated by factor '+str(standard_scaling_cont))
    standard_image_cont.writeto('E:/Science/OBS/SethOBS/LTT7379/mOBJ.068_flux.fits', clobber=True)

    #standard_image_cont[0].data = (standard_image_cont[0].data - standard_image_Ha[0].data)/standard_image_cont[0].data
    #standard_image_cont.writeto('E:/Science/OBS/2013-01-04/1020_diff.fits')

    y, x = np.ogrid[0:1488, 0:1500]
    
    obj_x_cen = 619
    obj_y_cen = 699
    obj_radius = 52
    obj_area = np.pi*obj_radius**2

    obj_bg_x_cen = 547
    obj_bg_y_cen = 855
    obj_bg_radius = 52
    obj_bg_area = np.pi*obj_bg_radius**2

    obj_exp_Ha = 1200.0
    obj_exp_cont = 720.0

    obj_mask = ((y - obj_y_cen)**2 + (x - obj_x_cen)**2) > obj_radius**2
    obj_bg_mask = ((y - obj_bg_y_cen)**2 + (x - obj_bg_x_cen)**2) > obj_bg_radius**2

    obj_image_Ha = pyfits.open('E:/Science/OBS/SethOBS/2014_Ha_on.fits')
    obj_image_Ha_bg = pyfits.open('E:/Science/OBS/SethOBS/2014_Ha_on.fits')
    obj_image_cont = pyfits.open('E:/Science/OBS/SethOBS/2014_Ha_off.fits')
    obj_image_cont_bg = pyfits.open('E:/Science/OBS/SethOBS/2014_Ha_off.fits')

    obj_image_Ha[0].data[obj_mask] = 0
    obj_image_Ha_bg[0].data[obj_bg_mask] = 0
    obj_image_cont[0].data[obj_mask] = 0
    obj_image_cont_bg[0].data[obj_bg_mask] = 0

    # Galactic Extinction
    extinction_mag_cont = 0.462  # CTIO filters from NED
    extinction_flux_cont = 10**(extinction_mag_cont/-2.5)
    
    extinction_mag_Ha = 0.316  # CTIO filters from NED
    extinction_flux_Ha = 10**(extinction_mag_Ha/-2.5)

    lum_distance = 741.8  # in Mpc
    lum_distance = lum_distance*3.08567758e24  # Mpc to cm
    lum_area = 4*np.pi*lum_distance**2
    obj_ha_flux = (np.sum(obj_image_Ha[0].data)-np.sum(obj_image_Ha_bg[0].data))*standard_scaling_Ha*lum_area/obj_exp_Ha
    obj_cont_flux = (np.sum(obj_image_cont[0].data)-np.sum(obj_image_cont_bg[0].data))*standard_scaling_cont*lum_area/obj_exp_cont

    print(obj_ha_flux, obj_cont_flux)

    #obj_image_Ha[0].data = obj_image_Ha[0].data - obj_image_cont[0].data*0.6
    #obj_image_Ha.writeto('E:/Science/OBS/2013-01-04/ngc1399_net.fits', clobber=True)
    
if __name__ == "__main__":
    main()
