#SOIwcs.cl created by Nathan De Lee 05/16/2006 all rights reserved.
#The WCS that is initially included with SOI images is often times wrong
#This script redoes the WCS.
#Revised 03/28/07 to fix searching for negative DECs

procedure soiwcs (input) 

string input {"",prompt="Input images"}
bool privcat {no,prompt="Use private catalog"}
bool catalog {"",prompt="Catalog File"}
int smin {"3",prompt="Radius is arcmin for catalog search"}
int search {"5",prompt="Msccmatch search parameter"}
bool verbose {yes,prompt="Verbose Output"}
bool mosaic {yes,prompt="Is this a Mosaic File"}
bool automat {yes,prompt="Use Msccmatch to updated WCS"}
bool fixhead {yes,prompt="Fix SOI imager header"}
bool usebpm {yes,prompt="Use bad pixel mask"}
struct *inlist

begin

string intemp,imgname,newname,catname,logname,test	
string m1,m2,m3,message
int rh,rm,dd,dm
int len,size1,size2
real rs,ds,ra,dec
	
#Get list of images
intemp = mktemp("temp")
files (input, sort=no, > intemp)
inlist = intemp

#Make sure MSCRED is loaded
if (!defpac("mscred")) {
        print("Package MSCRED is needed by soiwcs...please load it.")
        bye()
}

while (fscan (inlist, imgname) !=EOF) {

        #Check to see if images exists
        if(!imaccess(imgname)) {
                print (" File: "//imgname//" not found.")
                next;
        }

		#Get header information
		if(mosaic) {
			newname = imgname//"[1]"
		}
		else {
			newname = imgname
		}
		#Check to make sure I haven't done this already
		#I will only update plate scale crpix etc, if I haven't done this
		test = '0'
		hselect (newname, "wcscor",yes) | scan(test)
		
		if(test == '0' && fixhead) {
		#Get RA and DEC
		#The telescope actually points at TELRA and TELDEC
		imgets (newname, param="telra")
		print(imgets.value) | scanf("%d:%d:%f",rh,rm,rs)
		imgets (newname, param="teldec")
		print(imgets.value) |scanf("%d:%d:%f",dd,dm,ds)
		
		#Create new RA and DEC
		ra = (rh + rm / 60.0 + rs / 3600.0) * 15
		
	    if(substr(imgets.value,1,1) == "-"){
			dec = dd - dm / 60.0 - ds / 3600.0
		}
		else {
			dec = dd + dm /60.0 + ds / 3600.0
		}
		
	    #Fix Header Keywords
		if(mosaic) {
				ccdhedit(imgname,"crval1",ra,type="real")
				ccdhedit(imgname,"crval2",dec,type="real")
				hedit(imgname//"[1]","crpix1",1049.003,verify=no,add=no,\
					   addonly=no,show=no,delete=no,update=yes)			
				hedit(imgname//"[2]","crpix1",537.003,verify=no,add=no,\
					   addonly=no,show=no,delete=no,update=yes)			
				hedit(imgname//"[3]","crpix1",-24.997,verify=no,add=no,\
					   addonly=no,show=no,delete=no,update=yes)			
				hedit(imgname//"[4]","crpix1",-536.997,verify=no,add=no,\
					   addonly=no,show=no,delete=no,update=yes)			
				ccdhedit(imgname,"crpix2",1024,type="int")

				#I've updated this so that it just uses the correct value
			    ccdhedit(imgname,"cd1_1",-4.28E-05,type="real")
				ccdhedit(imgname,"cd2_1",0,type="real")
				ccdhedit(imgname,"cd1_2",0,type="real")
				ccdhedit(imgname,"cd2_2",4.28E-05,type="real")
				
			}				
		else {
			hedit(imgname,"crval1",ra,verify=no,add=no,addonly=no,show=no, \
				   delete=no,update=yes)
			hedit(imgname,"crval2",dec,verify=no,add=no,addonly=no,show=no, \
				   delete=no,update=yes)
			hedit(imgname,"crpix1",1024,verify=no,add=no,addonly=no,show=no, \
				   delete=no,update=yes)
			hedit(imgname,"crpix2",1024,verify=no,add=no,addonly=no,show=no, \
				   delete=no,update=yes) 
#I've updated this so that it just uses the correct value
		    ccdhedit(imgname,"cd1_1",-4.28E-05,type="real")
		    ccdhedit(imgname,"cd2_1",0,type="real")
			ccdhedit(imgname,"cd1_2",0,type="real")
			ccdhedit(imgname,"cd2_2",4.28E-05,type="real")
#			hedit(imgname,fields="cd1_1,cd1_2,cd2_1,cd2_2",value='($/2)', \
#				verify=no,add=no,addonly=no,show=no,delete=no,update=yes)
			}
		
#Create Message for working on keywords
			time | scanf("%s %s %s",m1,m2,m3)
			message = m1//" "//m2//" "//m3//": WCS Corrected"		
			if(mosaic) {
				ccdhedit(imgname,"wcscor",message,type="string")
			}
			else {
				hedit(imgname,"wcscor",message,verify=no,add=yes,addonly=no, \
					   show=no, delete=no,update=yes)
			}			
		}
#Get Catalog skip if using private catalog
		if(!privcat) {
			len = strldx(".",imgname)
			catname = substr(imgname,1,len) //"cat" 
			logname = substr(imgname,1,len) //"log"
#Mscgetcatalog won't overwrite things
			if(access(catname)) 
				delete(catname,verify=no)
			if(access(catname//1)) 
				delete(catname//1,verify=no)
			if(access(catname//2)) 
				delete(catname//2,verify=no)		

#Not all catalogs work for each image must try one or two
			mscgetcatalog(imgname,catname//1,magmin=10,magmax=20,\
						   catalog="usno2@noao",rmin=smin)
			mscgetcatalog(imgname,catname//2,magmin=10,magmax=20,\
						   catalog="usno2@cadc",rmin=smin)
#Check to see which catalog is better
			wc catname//1 | scan(size1)
			wc catname//2 | scan(size2)
			
			if(size1 != 0 && size1 >= size2) {
				rename(catname//1,catname,field="all")
				delete(catname//2,verify=no)
			}
			if(size2 != 0 && size1 < size2) {
				rename(catname//2,catname,field="all")
				delete(catname//1,verify=no)
			}
			if(size1 == 0 && size2 == 0) {
				delete(catname//1,verify=no)
				delete(catname//2,verify=no)
				print("Could not find catalog stars")
				next
			}
		}
		else {
			catname = catalog
		}	
		
#Create new WCS
		if(automat) {
			flpr
			if(access("temp.log")) {
				delete("temp.log",verify=no)
			}
			msccmatch (imgname,catname,verbose=yes,nsearch=100,\
						search=search,rsearch=0.3,cbox=11,maxshif=9,\
						csig=0.1,cfrac=0.5, listcoo=no,nfit=4,rms=.8,\
						fitgeom="general",reject=3,update=yes,interac=no,\
						accept=yes,usebpm=usebpm, >& "temp.log")
						
			if(verbose) {
				type("temp.log")
			}
			time | scanf("%s %s %s",m1,m2,m3)
			message = m1//" "//m2//" "//m3//": WCS Update Information:"
			print("\n"//message, >>&logname)
			type("temp.log", >>& logname)
			delete("temp.log",verify=no)
		}
	}
#Clean things up
inlist=""
delete(intemp,verify=no)
end
