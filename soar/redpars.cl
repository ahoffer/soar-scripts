#This script is the parameter file for the reduc script
#The values here are used by reduc.cl
#Created by Nathan De Lee 03/28/2007

procedure redpars ()
int fsize {9293760,prompt="Size of a complete file in bytes"}
bool check {yes,prompt="Check for complete files?"}
bool qtrim {yes,prompt="Execute Stage 1: Trim?"}
bool qzcom {yes,prompt="Execute Stage 2: Zero Combine?"}
bool qzcor {yes,prompt="Execute Stage 3: Zero Correction?"}
bool qfix  {yes,prompt="Execute Stage 4: Fix Headers?"}
bool qfcom {yes,prompt="Execute Stage 5: Flat Combine?"}
bool qfcor {yes,prompt="Execute Stage 6: Flat Correction"}
bool qicom {yes,prompt="Execute Stage 7: Image combine?"}
bool qrmfr {yes,prompt="Execute Stage 8: Remove Fringe I,i,z?"}
bool qwcs1 {no,prompt="Execute Stage 9: WCS Improvement for MEF?"}
bool qwcs2 {yes,prompt="Execute Stage 9: WCS Improvement for Mosaic? "}
string sdat {"sscripts$soi3.dat",prompt="Instrument file for MSCRED."}
string backup {"once",prompt="Backup data (none|once|all)?",enum="none|once|all"}
string bkuproo {"Raw/",prompt="Backup root (directory or prefix)"}
begin
epar("redpars")
end

