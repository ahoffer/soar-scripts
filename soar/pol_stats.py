def polarization_stats():
	try:
		import scipy.stats as stats
	except ImportError:
		print("Couldn't import scipy.stats.")
	try:
		import pyfits
	except ImportError:
		print("Couldn't import pyfits.")
	try:
		import numpy as np
	except ImportError:
		print("Couldn't import numpy.")
	try:
		import matplotlib.pyplot as plt 
	except ImportError:
		print("Couldn't import matplotlib.pyplot.")
	try:
		import pyregion
	except ImportError:
		print("Couldn't import pyregion.")
	try:
		from astroML.plotting import hist as astrohist
	except ImportError:
		print("Couldn't import astroML.plotting.")