#This script copies parameter files if necessary
if(!access("uparm$msdccdprc.par")) {
	copy("sscripts$param/msdccdprc.par","uparm")
}
;
if(!access("uparm$msdflatce.par")) {
	copy("sscripts$param/msdflatce.par","uparm")
}
;
if(!access("uparm$msdrmfrie.par")) {
	copy("sscripts$param/msdrmfrie.par","uparm")
}
;
if(!access("uparm$msdzeroce.par")) {
	copy("sscripts$param/msdzeroce.par","uparm")
}
;
if(!access("uparm$sorfixfit.par")) {
	copy("sscripts$param/sorfixfit.par","uparm")
}
;
if(!access("uparm$sorfixhed.par")) {
	copy("sscripts$param/sorfixhed.par","uparm")
}
;
if(!access("uparm$sorredpas.par")) {
	copy("sscripts$param/sorredpas.par","uparm")
}
;
if(!access("uparm$sorsoimoc.par")) {
	copy("sscripts$param/sorsoimoc.par","uparm")
}
;
if(!access("uparm$sorsoiwcs.par")) {
	copy("sscripts$param/sorsoiwcs.par","uparm")
}
;
