Updated 05/07/2007

This is a general guide for the installation and use of the SOI reduction
scripts.


If you have any questions, you can contact me at delee@pa.msu.edu or 
Sean Points (spoints@ctio.noao.edu).

Nathan De Lee

Installation:

I've set the scripts up like a package, so you need to add these lines to
your login.cl or hlib$extern.pkg:

set sscripts = path
task $soar=sscripts$soar.cl

Where path is the path to the directory where these scripts are stored.

Also, you must have the packages MSCRED and STSDAS installed on your reduction
machine.

Using the SOAR reduction scriptsscripts:

Step 1: Remove any bad images (i.e. saturated frames, incomplete frames, or 
frames you don't want to reduce.) from the directory you are working in.

Step 2: Use ccdlist *.fits to make sure that the obstype keyword is
set correctly in all of your images. The understood values are:
ZERO, SKYFLAT, OBJECT, and FLAT. (Remember since these are mosaic images you 
will have to use ccdhedit to alter all of the image extensions.)

Step 3: You will need to epar the task fixfilt before you
run reduc.cl, so that you can put in the positions of the filters.  The 
telescope operators can tell you what the position was for that night, or you
can look in the headers.

Step 4: Run or epar redpars and select which steps you would like to run.  The
fringe frames for I,i, and z are included with these scripts.

Step 5: Type reduc and all of the images in your directory will get reduced.


A quick list of what the scripts do:

soar.cl         The package script
redpars.cl      Allows you to set parameters for the reduc.cl task.
reduc.cl        The reduction script that calls all the others
fixfilt.cl      Translates the filter wheel position to filter names
fixhead.cl      Makes some corrections to the image headers (Still more to do)
soibpm.cl       A task to make bad pixel masks. (I don't currently use it.)
soimosaic.cl    A task to merge the amplifiers into one image.
soiwcs.cl       A task to correct the wcs keywords.

The outputs are:
image.fits      Reduced MEF image
mimage.fits     Reduced single image
image.cat       USNO catalog of the field (May or may not be produced)
image.log       Log of what soiwcs.cl did


Some quick notes:

The WCS on SOAR images are derived from the USNO catalog, and as such are
highly dependent on the number of stars in your field, which correspond with
that catalog.  Look more at soiwcs.cl if you would like to use your own 
catalog.  Also, always check the log files to make sure that the WCS 
succeeded.

Sometimes settings in other tasks can affect the pipeline.  In particular,
hedit and ccdhedit can cause troubles if their settings are wrong.  If the
script crashes I would suggest, unlearning those tasks.

The verboseness of the reduction scripts are determined by the MSCRED
verbose parameter.
