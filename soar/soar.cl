#Package SOAR

#Load necessary packages

#Define where SOAR dat file lives
mscred.instrum="sscripts$soi3.dat"

package soar

#SOAR Tasks

task $pcopy=sscripts$pcopy.cl
task $reduc=sscripts$reduc.cl
task redpars=sscripts$redpars.cl
task fixfilt=sscripts$fixfilt.cl
task soimosaic=sscripts$soimosaic.cl
task soiwcs=sscripts$soiwcs.cl
task soibpm=sscripts$soibpm.cl
task fixhead=sscripts$fixhead.cl

pcopy()
clbye()
