#Fixhead.cl created by Nathan De Lee 05/18/2006 all rights reserved.
#This script tries to correct header keyword problems found in the 
#current (Feb 2006) SOI headers.

procedure soiwcs (input) 

string input {"",prompt="Input images"}
bool mosaic  {yes,prompt="Is this a Mosaic File"}
bool detbin  {yes,prompt="Rewrite DETSEC in binned coordinate"}
struct *inlist
begin

string intemp,imgname
	
#Get list of images
intemp = mktemp("temp")
files (input, sort=no, > intemp)
inlist = intemp

#Make sure MSCRED is loaded
if (!defpac("mscred")) {
        print("Package MSCRED is needed by soiwcs...please load it.")
        bye()
}

while (fscan (inlist, imgname) !=EOF) {

        #Check to see if images exists
        if(!imaccess(imgname)) {
                print (" File: "//imgname//" not found.")
                next;
        }
#Correct header values for multi-extension images
		if(mosaic && detbin) {
#Fix DETSEC keyword
			hedit(imgname//"[1]","detsec","[1:512,1:2048]",verify=no,\
				   add=no,addonly=no,show=no,delete=no,update=yes)			
			hedit(imgname//"[2]","detsec","[513:1024,1:2048]",verify=no,\
				   add=no,addonly=no,show=no,delete=no,update=yes)			
			hedit(imgname//"[3]","detsec","[1025:1536,1:2048]",verify=no,\
				   add=no,addonly=no,show=no,delete=no,update=yes)		
			hedit(imgname//"[4]","detsec","[1537:2048,1:2048]",verify=no,\
				   add=no,addonly=no,show=no,delete=no,update=yes)
		}
		if(mosaic) {
#Fix CCDNAME keyword
			hedit(imgname//"[1]","ccdname","ccd1",verify=no,add=no,\
				   addonly=no,show=no,delete=no,update=yes)			
			hedit(imgname//"[2]","ccdname","ccd1",verify=no,add=no,\
				   addonly=no,show=no,delete=no,update=yes)			
			hedit(imgname//"[3]","ccdname","ccd2",verify=no,add=no,\
				   addonly=no,show=no,delete=no,update=yes)			
			hedit(imgname//"[4]","ccdname","ccd2",verify=no,add=no,\
				   addonly=no,show=no,delete=no,update=yes)			

		}
#Fix regular header
		if(!mosaic)	{
		hedit(imgname,"waxmap01",verify=no,add=no,\
				   addonly=no,show=no,delete=yes,update=yes)
		}
	}
#Clean things up
inlist=""
delete(intemp,verify=no)
end
