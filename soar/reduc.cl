#Reduc.cl created by Nathan De Lee 03/28/2007 based on a script of the same
#name created by Eduardo Cypriano 2005-04-24
#This script has paramters which can be set by epar redpars
#Updated to handle new headers, and multiple fringe frames.
#Updated to copy needed parameter files 5-18-2007 ND

#Must load mscred
if(defpac("mscred")==no) {
	print("Loading mscred...")
	mscred
}
;
mscred.instrum=redpars.sdat
mscred.backup=redpars.backup
mscred.bkuproo=redpars.bkuproo
if(defpac("mscred")==no) {
  print("ERROR: The MSCRED package is required but not defined.")
  clbye()
}
;
#Must load stsdas
if(defpac("stsdas")==no) {
	print("Loading stsdas...")
	stsdas
}
;
if(defpac("stsdas")==no) {
	print("ERROR: The STSDAS package is required but not defined.")
    clbye()
}
;

#Check for correct parameter fils
pcopy()

if(redpars.check) {
	#Check to make sure all the images are good
	print ("ls -l *.fits | awk '{if($5 != "//redpars.fsize//") print}' | \
	grep -v 'Flat' |grep -v 'Zero'> good.tmp") | sh
	!wc good.tmp | awk '{if($1 == 0) print("rm -f good.tmp")}' | sh
	if (access("good.tmp")==yes) {
		print("The following files do not have the correct size:")
		cat good.tmp
		!rm -f good.tmp
		clbye()
	}
	;
}
;
if(redpars.qtrim) {
!echo "Stage 1: Trim"
	ccdproc("*.fits",fixpix=no,oversca=yes,trim=yes,zerocor=no,\
	darkcor=no,flatcor=no,sflatco=no,split=no,merge=no,biassec="image",\
	trimsec="image",ccdtype="")
}
;	
if(redpars.qzcom) {
	!echo "Stage 2: Zero Combine"
	if (imaccess("Zero.fits")==yes) {
  		print("Removing Zero.fits")
  		!rm -f Zero.fits
	}
	;	
	zerocombine("*.fits",ccdtype="zero",output="Zero",combine="median",\
	reject="minmax",nlow=1,nhigh=1,process=no)
}
;
if(redpars.qzcor) {
	!echo "Stage 3: Zero Correction"
	ccdproc("*.fits",ccdtype="",oversca=no,trim=no,zerocor=yes,darkcor=no,\
		flatcor=no,sflatco=no,split=no,merge=no,zero="Zero",\
		biassec="image",trimsec="image",fixpix=no,ccdtype="")

}
;
if(redpars.qfix) {
	!echo "Stage 4: Fix Headers"
	fixhead("*.fits",mosaic=yes,detbin=no)
	fixfilt("*.fits",newhead="FILTER")
}
;
if(redpars.qfcom) {
	!echo "Stage 5: Flat Combine"
#Look in the fixfilt parameter file and remove any leftover master 
#flat files
	if (imaccess("Flat"//fixfilt.w1filt1//".fits")==yes) {
  		print("Removing Flat"//fixfilt.w1filt1//".fits")
  		delete("Flat"//fixfilt.w1filt1//".fits",verify=no)
	}
	;
	if (imaccess("Flat"//fixfilt.w1filt2//".fits")==yes) {
  		print("Removing Flat"//fixfilt.w1filt2//".fits")
  		delete("Flat"//fixfilt.w1filt2//".fits",verify=no)
	}
	;
	if (imaccess("Flat"//fixfilt.w1filt3//".fits")==yes) {
  		print("Removing Flat"//fixfilt.w1filt3//".fits")
  		delete("Flat"//fixfilt.w1filt3//".fits",verify=no)
	}
	;
	if (imaccess("Flat"//fixfilt.w1filt4//".fits")==yes) {
  		print("Removing Flat"//fixfilt.w1filt4//".fits")
  		delete("Flat"//fixfilt.w1filt4//".fits",verify=no)
	}
	;
	if (imaccess("Flat"//fixfilt.w1filt5//".fits")==yes) {
  		print("Removing Flat"//fixfilt.w1filt5//".fits")
  		delete("Flat"//fixfilt.w1filt5//".fits",verify=no)
	}
	;
	if (imaccess("Flat"//fixfilt.w2filt1//".fits")==yes) {
  		print("Removing Flat"//fixfilt.w2filt1//".fits")
  		delete("Flat"//fixfilt.w2filt1//".fits",verify=no)
	}
	;
	if (imaccess("Flat"//fixfilt.w2filt2//".fits")==yes) {
  		print("Removing Flat"//fixfilt.w2filt2//".fits")
  		delete("Flat"//fixfilt.w2filt2//".fits",verify=no)
	}
	;
	if (imaccess("Flat"//fixfilt.w2filt3//".fits")==yes) {
  		print("Removing Flat"//fixfilt.w2filt3//".fits")
  		delete("Flat"//fixfilt.w2filt3//".fits",verify=no)
	}
	;
	if (imaccess("Flat"//fixfilt.w2filt4//".fits")==yes) {
  		print("Removing Flat"//fixfilt.w2filt4//".fits")
  		delete("Flat"//fixfilt.w2filt4//".fits",verify=no)
	}
	;
	if (imaccess("Flat"//fixfilt.w2filt5//".fits")==yes) {
  		print("Removing Flat"//fixfilt.w2filt5//".fits")
  		delete("Flat"//fixfilt.w2filt5//".fits",verify=no)
	}
	;
	flatcombine("*.fits",ccdtype="flat",output="Flat",combine="median",\
			reject="minmax",nlow=1,nhigh=1,process=no)
}
;
if(redpars.qfcor) {
	!echo "Stage 6: Flat Correction"
	ccdproc("*.fits",oversca=no,trim=no,zerocor=no,darkcor=no,\
		flatcor=yes,sflatco=no,split=no,merge=no,flat="Flat*",\
		biassec="image",trimsec="image",fixpix=no,ccdtype="")
}
;
if(redpars.qicom) {
	!echo "Stage 7: Image Combine"
	#Check for soimosaic task
	if (deftask("soimosaic")==no) {
		task soimosaic=sscripts$soimosaic.cl
	}
	;
	hselect ("*.fits[0]", "$I", 'obstype == "OBJECT"',>"tmp123")
	!sed 's/\[0\]//g' tmp123 > tmp321
	soimosaic("@tmp321",outimag="",outpref="m",xgap=102,ygap=5,tilt=0)
	#Fix headers of new images
	fixhead("m*.fits",mosaic=no)
}
;
if(redpars.qrmfr) {
	!echo "Stage 8: Remove fringes in I,i,z frames"
#Select I images
hselect("m*.fits","$I",'filter=="I"', >"tmp111")
rmfringe(input="@tmp111",output="",fringe="sscripts$FringeI.fits",masks="",\
		fringem="",background="",ncblk=5,nlblk=5,extfit="",logfile="logfile",\
		verbose=mscred.verbose)
delete("tmp111",verify=no)
hselect("m*.fits","$I",'filter=="i"', >"tmp111")
rmfringe(input="@tmp111",output="",fringe="sscripts$Fringei.fits",masks="",\
		fringem="",background="",ncblk=5,nlblk=5,extfit="",logfile="logfile",\
		verbose=mscred.verbose)
delete("tmp111",verify=no)
hselect("m*.fits","$I",'filter=="z"', >"tmp111")
rmfringe(input="@tmp111",output="",fringe="sscripts$Fringez.fits",masks="",\
		fringem="",background="",ncblk=5,nlblk=5,extfit="",logfile="logfile",\
		verbose=mscred.verbose)
delete("tmp111",verify=no)
}
;
if(redpars.qwcs1) {
	!echo "Stage 9: WCS improvement for MEF"
	soiwcs("@tmp321",smin=3,search=9,verbose=mscred.verbose,mosaic=yes,usebpm=no,fixhead=yes,automat=yes)
}
;
if(redpars.qwcs2) {
	!echo "Stage 9: WCS improvement for Mosaic"
	soiwcs("m*.fits",smin=5,search=20,verbose=mscred.verbose,mosaic=no,usebpm=no,fixhead=yes,automat=yes)
}	
;
if (access("tmp123")==yes) {
	delete("tmp123",verify=no)
}
;
if (access("tmp321")==yes) {
	delete("tmp321",verify=no)
}
;
